import init, { run_app } from './pkg/sketchy.js';
import "./css/styles.css";

async function main() {
   await init('/pkg/sketchy_bg.wasm');
   run_app();
}
main()
