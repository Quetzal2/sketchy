pub struct Brush {
    pub size: f32,
    pub opacity: f32,
}
