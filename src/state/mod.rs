pub mod canvas;
pub mod tools;
pub mod ui;


pub struct State {
    pub canvas: canvas::Canvas,
    pub toolbox: tools::Tool,
    pub ui: ui::UI,
}
