use web_sys::{WebGlFramebuffer, WebGlTexture};

pub struct Layer {    /// The opacity of the layer
    pub opacity: f64,

    /// The WebGL texture
    pub texture: WebGlTexture,

    /// The WebGL framebuffer for the texture
    pub framebuffer: WebGlFramebuffer,

    /// The blend mode
    pub blend_mode: LayerBlendMode,

    /// The width of the layer
    pub width: u32,

    /// The height of the layer
    pub height: u32,
}


pub enum LayerBlendMode {
    AlphaOver,
}
