mod layer;

pub struct Canvas {
    pub layers: Vec<layer::Layer>,
    pub screen: layer::Layer,
}
