pub mod canvas;
pub mod tools;
pub mod ui;

use yew::{function_component, html};

#[function_component(Interface)]
pub fn interface() -> Html {
    html! {
        <ui::UI/>
    }
}
