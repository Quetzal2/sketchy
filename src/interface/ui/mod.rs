use core::fmt;

use super::canvas::Canvas;
use super::tools::Tools;
use std::collections::HashMap;

use yew::{Component, function_component, html};

#[function_component(UI)]
pub fn ui() -> Html {
    let pos_x = 200;
    let pos_y = 300;
    let style = format!("position: absolute; top: {:?}px; left: {:?}px;display: flex;flex-direction: column;background-color: #224232", pos_x, pos_y);
    html! {<>
        <Canvas/>
        <div {style}>
            <div style="height: 10px; background-color: #557565;"></div>
            <Tools/>
        </div>
    </>}
}

