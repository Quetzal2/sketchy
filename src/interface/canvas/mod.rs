

use yew::{NodeRef, function_component, html, use_state};

#[function_component(Canvas)]
pub fn canvas() -> Html {
    //            <canvas ref=self.node_ref.clone() onpointerdown=toggle_pressed onpointerup=toggle_pressed2 onpointermove=drawpoint width=800 height=600 style="border: solid black 5px; width: 800px; height: 600px;"/>  /*\!/ Orphan canvas sizes /!\*/
    //let onpointerdown = ;
    //{onpointerdown} {onpointerup} {onpointermove} {width} {height} {style}
    let state = use_state(||{NodeRef::default()});

    html! { <canvas ref={(*state).clone()} /> }
}
