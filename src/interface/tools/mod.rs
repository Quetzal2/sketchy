

use yew::{function_component, html};

#[function_component(Tools)]
pub fn tools() -> Html {
    //onchange=size_changed  style="float: left;"
    html! {<>
        <input type="color" value="#ff07ae" class="slider" orient="horizontal"/>
        <input type="range" min="0.0" max="0.1" step="0.001" value="0.003" class="slider" orient="horizontal"/>
    </>}
}
